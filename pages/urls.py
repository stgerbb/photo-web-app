from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('services/', views.ServicesView.as_view(), name='services'),
    path('about/', views.AboutView.as_view(), name='about'),
    path('loaderio-5732a184e2e50ef77e4fee1507331183/', views.LoaderioView.as_view(), name='loaderio'),
]