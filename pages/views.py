from django.views.generic import TemplateView

# Create your views here.
class HomeView(TemplateView):
    template_name = 'index.html'

class ServicesView(TemplateView):
    template_name = 'services.html'

class AboutView(TemplateView):
    template_name = 'about.html'

class LoaderioView(TemplateView):
    template_name = 'loaderio-5732a184e2e50ef77e4fee1507331183.txt'