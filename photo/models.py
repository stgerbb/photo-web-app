from django.db import models
from django.urls import reverse
from django.dispatch import receiver
import os
from PIL import Image

# Create your models here.
def photo_directory_path(instance, filename):
    return 'photo_uploads/{0}/album_{1}/{2}'.format(instance.album.client.username, instance.album.title, filename)

class Album(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    date_created = models.DateField()
    album_cover = models.ImageField(default='default.jpg', upload_to='album_covers')
    client = models.ForeignKey(
        'account.CustomUser',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('album_detail', args=[str(self.id)])

class Photo(models.Model):
    photo_name = models.CharField(max_length=200)
    album = models.ForeignKey(
        Album,
        on_delete=models.CASCADE
    )
    image = models.ImageField(upload_to=photo_directory_path)

    def __str__(self):
        return self.photo_name

@receiver(models.signals.post_delete, sender=Photo)
def auto_delete_image_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem (image)
    when corresponding `MediaFile` object is deleted.
    """
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)

@receiver(models.signals.post_delete, sender=Album)
def auto_delete_albumcover_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem (albumcover)
    when corresponding `MediaFile` object is deleted.
    """
    if instance.album_cover:
        if 'default.jpg' not in instance.album_cover.path:
            if os.path.isfile(instance.album_cover.path):
                os.remove(instance.album_cover.path)

@receiver(models.signals.pre_save, sender=Photo)
def auto_delete_image_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem (image)
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).image
    except sender.DoesNotExist:
        return False

    new_file = instance.image
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)

@receiver(models.signals.pre_save, sender=Album)
def auto_delete_albumcover_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem (albumcover)
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).album_cover
    except sender.DoesNotExist:
        return False

    new_file = instance.album_cover
    if not old_file == new_file:
        if 'default.jpg' not in old_file.path:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)