from django.views.generic import ListView, DetailView
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from account.models import CustomUser
from .models import Album, Photo

# Create your views here.
class AlbumsListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Album
    template_name = 'albums.html'
    login_url = '/account/login/'

    def get_queryset(self):
        # Get the user from url parameter
        user = get_object_or_404(CustomUser, username=self.kwargs.get('username'))
        # Return all the album object of that specific user
        return Album.objects.filter(client=user).order_by('-date_created')

    def test_func(self):
        user = get_object_or_404(CustomUser, username=self.kwargs.get('username'))
        if self.request.user == user:
            return True
        return False

class AlbumDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Album
    template_name = 'album_detail.html'
    login_url = '/account/login/'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the photos
        context['photo_list'] = Photo.objects.all().filter(album=self.object)
        return context

    def test_func(self):
        album = self.get_object()
        if self.request.user == album.client:
            return True
        return False