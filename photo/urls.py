from django.urls import path
from . import views

urlpatterns = [
    path('<str:username>/', views.AlbumsListView.as_view(), name='albums'),
    path('<int:pk>/photos/', views.AlbumDetailView.as_view(), name='album_detail'),
]