from django.contrib import admin
from .models import Album, Photo
from django.core.files.images import ImageFile

# Register your models here.
class PhotoInline(admin.StackedInline):
    model = Photo
    extra = 1
    list_display = ('photo_name', 'image',)
    verbose_name = "Photo"
    verbose_name_plural = "Upload individual photos"

class AlbumAdmin(admin.ModelAdmin):
    inlines = [
        PhotoInline,
    ]

    def save_model(self, request, obj, form, change):
        obj.save()

        for photo in request.FILES.getlist('multiple_photo_upload'):
            photo_model = Photo(photo_name=photo.name, image=photo, album=obj)
            photo_model.save()

admin.site.register(Album, AlbumAdmin)