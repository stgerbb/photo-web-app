from django.urls import path
#from django.contrib.auth.views import PasswordChangeView
from . import views

urlpatterns = [
    path('<int:pk>/settings/', views.AccountSettings.as_view(), name='account_settings'),
    path('<int:pk>/settings/password/', views.ChangePasswordView.as_view(), name="password_change"),
    path('<int:pk>/settings/delete/', views.DeleteAccountView.as_view(), name="account_delete"),
    path('signup/', views.SignUpView.as_view(), name='signup'),
]