from django.views.generic import TemplateView
from .forms import CustomUserCreationForm, CustomUserChangeForm
from django.views.generic.edit import DeleteView, UpdateView, CreateView
from django.urls import reverse_lazy
from .models import CustomUser
from django.contrib.auth.views import PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages

# Create your views here.
class AccountSettings(SuccessMessageMixin, UpdateView):
    model = CustomUser
    form_class = CustomUserChangeForm
    template_name = 'account_settings.html'
    success_message = "Your changes were saved successfully."

    def get_success_url(self):
          pk = self.kwargs['pk']
          return reverse_lazy('account_settings', kwargs={'pk': pk})

class SignUpView(SuccessMessageMixin, CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
    success_message = "Account was created successfully. You can now log in."

class ChangePasswordView(SuccessMessageMixin, PasswordChangeView):
    success_message = "Password was changed successfully."

    def get_success_url(self):
          pk = self.kwargs['pk']
          return reverse_lazy('account_settings', kwargs={'pk': pk})

class DeleteAccountView(SuccessMessageMixin, DeleteView):
    model = CustomUser
    template_name = 'account_delete.html'
    success_url = reverse_lazy('home')
    success_message = "Account was deleted successfully."

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(DeleteAccountView, self).delete(request, *args, **kwargs)