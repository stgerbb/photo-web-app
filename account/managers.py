from django.contrib.auth.base_user import BaseUserManager
from django.utils import timezone

class CustomUserManager(BaseUserManager):
    def _create_user(self, email, username, password, is_staff, is_superuser, **extra_fields):
        now = timezone.localtime()
        if not email:
            raise ValueError('The email must be set.')
        email = self.normalize_email(email)
        user = self.model(email=email,
                            username=username,
                            is_staff=is_staff,
                            is_superuser=is_superuser,
                            last_login=now,
                            date_joined=now,
                            **extra_fields)
        user.set_password(password)
        user.save()

    def create_user(self, email, username, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, username, password, **extra_fields):
        return self._create_user(email, username, password, True, True, **extra_fields)